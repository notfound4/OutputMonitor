#include <boost/program_options.hpp>
#include <iostream>

#include "Config.hpp"
#include "Monitor.hpp"

namespace po = boost::program_options;

int main (int argc, char *argv[])
{
	std::vector<std::string> commands, unique_commands;

	// Declare the supported options.
	try {
		po::options_description desc("Allowed options");
		desc.add_options()
		("help,h", "produce help message")
		("version,v", "print version string")
		("commands,c", po::value<std::vector<std::string> >()->multitoken(),
			"list of commands to launch when this program detect a change in outputs.")
		("unique-commands,u", po::value<std::vector<std::string> >()->multitoken(),
			"list of commands to launch when this program detect a change in outputs. Commands are launch once when changes are detected. They are launched before the other commands.")
		;

		po::positional_options_description pos_desc;

		po::variables_map vm;
		po::store(po::command_line_parser(argc, argv).options(desc).positional(pos_desc).allow_unregistered().run(),
			vm);
		po::notify(vm);

		if (vm.count("help")) {
			std::cout << desc << std::endl;
			return 1;
		}

		if (vm.count("version")) {
			std::cout << "outmon verion: " << OutputMonitor_VERSION_MAJOR << "." <<
			OutputMonitor_VERSION_MINOR << std::endl;
			return 1;
		}

		if (vm.count("commands")){
			commands = vm["commands"].as<std::vector<std::string>>();
		}

		if (vm.count("unique-commands")){
			unique_commands = vm["unique-commands"].as<std::vector<std::string>>();
		}
	}
	catch(po::error& e) {
		std::cerr << "error: " << e.what() << std::endl;
		std::cerr << "Use --help to get info on how to use daemon404." << std::endl;
		return 1;
	}

	Monitor mon(commands, unique_commands);
	mon.run();

	return EXIT_SUCCESS;
}