#pragma once

#include "i3ipc++/ipc.hpp"

class Monitor {
public:
	Monitor() = delete;
	Monitor(const std::vector<std::string> &commands, const std::vector<std::string> &unique_commands);
	~Monitor();
	Monitor(Monitor const &) = delete;
	Monitor &operator=(Monitor const &) = delete;

	void run();

	void update_outputs(std::vector<std::shared_ptr<i3ipc::output_t>> outputs);
private:
	i3ipc::connection m_conn;
	std::vector<std::shared_ptr<i3ipc::output_t>> m_outputs;
	std::vector<std::string> m_commands;
	std::vector<std::string> m_unique_commands;
};