#include "Monitor.hpp"

#include <algorithm>
#include <cstdlib>
#include <iostream>

Monitor::Monitor(const std::vector<std::string> &commands, const std::vector<std::string> &unique_commands)
: m_commands(commands), m_unique_commands(unique_commands)
{
	// Get all the active outputs at launch
	std::vector< std::shared_ptr< i3ipc::output_t > > outputs = m_conn.get_outputs();
	for (auto&  output : outputs) {
		if (output->active) {
			m_outputs.push_back(output);
		}
	}
	update_outputs(m_outputs);

	m_conn.subscribe(i3ipc::ET_OUTPUT);
	m_conn.signal_output_event.connect([&](){
		std::vector< std::shared_ptr< i3ipc::output_t > > outputs = m_conn.get_outputs();
		std::vector< std::shared_ptr< i3ipc::output_t > > future_outputs;
		for (auto&  output : outputs) {
			if (output->active) {
				future_outputs.push_back(output);
			}
		}
		if (future_outputs.size() == m_outputs.size()) {
			for (auto&  output : future_outputs) {
				if ( std::find_if(m_outputs.begin(), m_outputs.end(),
					[&](std::shared_ptr<i3ipc::output_t> const& output_old){
						return output->name == output_old->name &&
						output->rect.x == output_old->rect.x &&
						output->rect.y == output_old->rect.y &&
						output->rect.width == output_old->rect.width &&
						output->rect.height == output_old->rect.height;
					}) == m_outputs.end()) {
					update_outputs(future_outputs);
					return;
				}
			}
		}
		else
			update_outputs(future_outputs);
	});
}

Monitor::~Monitor() {

}

void Monitor::run() {
	while (true) {
		m_conn.handle_event();
	}
}

void Monitor::update_outputs(std::vector<std::shared_ptr<i3ipc::output_t>> outputs) {
	for (auto& command : m_unique_commands) {
		if (system(command.c_str()))
			std::cout << command.c_str() << " failed to execute" << std::endl;
	}

	m_outputs = outputs;
	for (auto& output : m_outputs) {
		for (auto command : m_commands) {
			while (command.find("%name%") != std::string::npos){
				command.replace(command.find("%name%"), 6, output->name);
			}
			while (command.find("%x%") != std::string::npos){
				command.replace(command.find("%x%"), 3, std::to_string(output->rect.x));
			}
			while (command.find("%y%") != std::string::npos){
				command.replace(command.find("%y%"), 3, std::to_string(output->rect.y));
			}
			while (command.find("%width%") != std::string::npos){
				command.replace(command.find("%width%"), 7, std::to_string(output->rect.width));
			}
			while (command.find("%height%") != std::string::npos){
				command.replace(command.find("%height%"), 8, std::to_string(output->rect.height));
			}
			while (command.find("%primary%") != std::string::npos){
				command.replace(command.find("%primary%"), 9, std::to_string(output->primary));
			}
			if ( system(command.c_str()) ) {
				std::cout << command.c_str() << " failed to execute" << std::endl;
			}
		}
	}
}